require 'client.rb'

class TweetsController < ApplicationController
  def index
    # Get the keyword from tweets#new
    @keyword = params[:query]
    t = MyTwitter.new
    @responses = t.getTenTweets(@keyword)
    @responses.each do |response|
      # Need to add de dupe here
      Tweet.create(text: response.text, username:response.user.screen_name)
    end
    @tweets = Tweet.last(10)
  end

  def new
  end


  def create
    @newTweet = params[:query]
    t2 = MyTwitter.new
    postTweet(@newTweet)

    # Re route to homepage tweets/new
  end

end
