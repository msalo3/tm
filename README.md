# README

* Ruby version : 2.3.1

* Deployed via Heroku using Postgres DB

* To successfully deploy, ensure your twitter API keys are properly placed in ./lib/client.rb

* gems needed:
  - gem install twitter
  - gem install pg
  - gem install bootstrap-sass

* MyTwitter class in client.rb controls communication with the twitter API for READ ONLY
  - To add POST options, see http://www.rubydoc.info/gems/twitter for instructions

* Currently the service provides ALL tweets about a topic

* Other items to still be completed:
  - De-Duplication of database
    - tweets should be run through a de dupe process based on tweet id before storage
    - Need to add tweet id column to Tweets table
    - Need to pass newest DB tweetID into twitter connector to only grab tweets newer than that id
  - Filtering of retweets
    - Current filtering is not functional yet as the rate limit is hit before a full 10 original tweets are able to be amassed
    - See getUniqueTweets() in client.rb for more
